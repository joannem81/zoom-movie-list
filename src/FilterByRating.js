import React from 'react';
import PropTypes from 'prop-types';
import './styles/components/filter-rating.scss';

function FilterByRating({ rating, movieCount, onRatingChange }) {
  return (
    <form className="filter-rating">
    <label className="filter-rating__label" htmlFor="movie-rating">
      Choose a rating between 0 to 10 to filter the list of movies:
    </label>
    <input className="filter-rating__range"type="range" id="movie-rating" min="0" max="10" step="0.5"
      defaultValue={rating} onChange={onRatingChange} />
    <p>{movieCount} movies found with a rating of at least {rating}.</p>
  </form>
  );
};

FilterByRating.propTypes = {
  rating: PropTypes.number.isRequired,
  movieCount: PropTypes.number.isRequired
};

export default FilterByRating;
