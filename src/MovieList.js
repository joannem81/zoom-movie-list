
import React from 'react';
import PropTypes from 'prop-types';
import './styles/components/movie-list.scss';

function MovieList({ items }) {
  return (
    <ol className="movie-list">
      {items.map(item =>
      <li key={item.id} className="movie-list__item">
        <h2 className="movie-list__title">{item.title}</h2>
        <img src={item.image_url} width={item.image_width} alt="" className="movie-list__image" />
        <div className="movie-list__genres">
          <h3 className="movie-list__genre-title">Genres:</h3>
          <p className="movie-list__genre-text">{item.genres} </p>
        </div>
      </li>
      )}
    </ol>
  );
}

MovieList.propTypes = {
  items: PropTypes.array.isRequired
};

export default MovieList;
