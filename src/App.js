import React from 'react';
import axios from 'axios'
import './styles/base.scss';
import MovieList from './MovieList';
import FilterByRating from './FilterByRating';

class App extends React.Component {

  state = {
    apiKey: '1c9963a7944d1d40bf9117eb0d6bf44e',
    apiURL: 'https://api.themoviedb.org/3/',
    rating: 3,
    moviesOriginal: [],
    moviesFiltered: [],
    imageConfig: {},
    genres: []
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    // request data required to display images, genre names and now playing list
    axios.all([
      axios.get(`${this.state.apiURL}configuration?api_key=${this.state.apiKey}`),
      axios.get(`${this.state.apiURL}genre/movie/list?api_key=${this.state.apiKey}&language=en-US`),
      axios.get(`${this.state.apiURL}movie/now_playing?api_key=${this.state.apiKey}&language=en-US&page=1`)
    ])
    .then((responses)=>{
      // save data
      this.setState({ 
        imageConfig: responses[0].data.images,
        genres: responses[1].data.genres,
        moviesOriginal: responses[2].data.results }, function() {
          // setState is complete
          this.updateMovieData();
        });
    })
  }

  updateMovieData = () => {
    // sort movies by descending popularity order
    this.sortMovies('popularity', 'descending');

    this.addGenreNames();

    // initial filter required to exclude any below default value
    this.filterMoviesByRating(this.state.rating);
  }

  sortMovies = (sortBy, sortOrder) => {
    if (sortOrder === 'descending') {
      this.state.moviesOriginal.sort((a,b)=>b[sortBy]-a[sortBy]);
    } else { // ascending
      this.state.moviesOriginal.sort((a,b)=>a[sortBy]-b[sortBy]);
    }
  }

  addGenreNames() {
    // movies only contain genre IDs, add the names from the genre API.

    // map movies to new array with genres and image info added
    let moviesNew = this.state.moviesOriginal.map((movie) => {
      const genreCount = movie.genre_ids.length - 1;
      let genreNames = '';
      // loop through all genres for each movie
      movie.genre_ids.forEach((genreID, index) => {
        // find genre for this ID
        const matchedGenre = this.state.genres.find(genre => genre.id === genreID);
        // add a comma after all items except last
        const trailingComma = (genreCount === index) ? '' : ', ';
        genreNames += `${matchedGenre.name}${trailingComma}`;
      });
      // add genre names to movie
      movie.genres = genreNames;
      this.addImageInfo(movie);
      return movie;
    });

    this.setState({ moviesOriginal: moviesNew });
  }

  addImageInfo = (movie) => {
    // add image data to movie, created from configuration and now_playing APIs
    movie.image_url = `${this.state.imageConfig.base_url}${this.state.imageConfig.poster_sizes[1]}${movie.poster_path}`;
    movie.image_width = `${this.state.imageConfig.poster_sizes[1].slice(1)}`;
  }

  filterMoviesByRating = (newRating) => {
    const filteredData = this.state.moviesOriginal.filter((movie) => movie.vote_average >= newRating);
    // update state for fitered now playing list
    this.setState({ moviesFiltered: filteredData });
  }

  handleRatingUpdate = (event) => {
    // update rating state
    this.setState({ rating: parseFloat(event.target.value) }, function() {
      // setState is complete
      this.filterMoviesByRating(this.state.rating);
    });
  }

  render() {
    return (
      <main className="wrapper">
        <h1 className="heading1">Now playing list from the movie db</h1>
        <FilterByRating
          rating={this.state.rating}
          movieCount={this.state.moviesFiltered.length}
          onRatingChange={this.handleRatingUpdate} />

        <MovieList items={this.state.moviesFiltered} />
      </main>
    );
  }
}

export default App;
